# Zweck

Dieses Repository verwendet die [TAL template engine](https://github.com/jhiver/template-tal#readme) als XML-Präprozessor, um die _Übersichtlichkeit_ und _Wartbarkeit_ des Standard-Kartenstils [Elevate](https://www.openandromaps.org/kartenlegende/elevation-hike-theme) von OpenAndroMaps zu erhöhen. Ziel ist es auch seine _Erweiterbarkeit_ zu vereinfachen, um die Erstellung einer Familie von ähnlichen Elevate-Kartenstilen zu ermöglichen.

Möglichkeiten:

- Aufteilen in logische Einheiten:

  Die über 7000 Zeilen lange Original-Datei ([original/Elevate.xml](original/Elevate.xml) und [original/Elements.xml](original/Elements.xml)) kann mit `<tal:include>` und [tal:replace](https://github.com/jhiver/template-tal#supported-tal-statements) in logische Einheiten aufgeteilt werden (siehe z.B. [Template.xml](Template.xml)),

- Wiederverwenden von Code-Wiederholungen:

  Die wiederholten Vorkommen der verschieden Einstellungs-Layer für jeden der Unterkartenstile `Wandern`, `Stadt`, `Radfahren` und `Mountainbike` müssen nur einmal definiert und können mehrfach inkludiert werden (siehe [part/stylemenu.xml](part/stylemenu.xml))

- Generieren von Wiederholungen mit leichten Unterschieden:

  Die Wegmarkierungen bestehen aus sehr viel gleichem Code, der sich nur an wenigen Stellen unterscheidet. Mit [tal:repeat](https://github.com/jhiver/template-tal#supported-tal-statements) und einer Daten-Datei (siehe [ElevateData.js](ElevateData.js)) kann man deutlicher machen, was sich jeweils unterscheidet bzw. gleich ist (siehe [part/waymark.xml](part/waymark.xml)).

- Abfrage von Flags für Varianten:

  `Elevate` und `Elements` unterscheiden sich nur in teilweise fehlenden `zoom-min`-Angaben und einem unterschiedlichen ID-Präfix (`elmt-` statt `elv-`). Das kann durch Überschreiben der jeweiligen Werte in der Daten-Datei (siehe [ElementsData.js](ElementsData.js)) konfiguriert werden und im XML z.B. mit [tal:attributes](https://github.com/jhiver/template-tal#supported-tal-statements) abgefragt werden (siehe z.B. [part/underground_pipelines.xml](part/underground_pipelines.xml)). Ähnliche Flags und weitere Abfragen (z.B. mit [tal:condition](https://github.com/jhiver/template-tal#supported-tal-statements) könnte man zukünftig für weitere Varianten der Elevate-Familie verwenden.

# Generieren

1. Installieren von [node.js](https://nodejs.org) (minimale Ausführungsumgebung für JavaScript)
1. [Herunterladen](https://gitlab.com/winni/oam-elevate/-/archive/main/oam-elevate-main.zip) und entpacken diese Projekts (Falls Git installiert ist, ist es am besten und einfachsten stattdessen dieses Repository zu clonen)

1. Öffnen einer Konsole im entpackten Ordner `oam-elevate-main` (bzw. im ge-clone-ten Ordner `oam-elevate`)
1. `npm install`
1. `npm run generate`

# Editieren

Man kann die XML- und Datendateien mit einem beliebigen Editor bearbeiten, es wird jedoch empfohlen, einen XML-Schema-fähigen Editor (für Validierung und Content-Assist) zu verwenden. Am einfachsten ist es, die beschriebenen Schritte zu befolgen:

1. Installieren von [Visual Studio Code](https://code.visualstudio.com/)
1. Visual Studo Code starten und den Ordner `oam-elvate` mit `File -> Open Folder ...` öffnen
1. Die empfohlenen Erweiterungen (XML Language Support) installieren, die einem beim Öffnen des Ordners mit Visual Studio Code in einem Popup angeboten werden
1. Beliebige XML- oder JS-Quell-Dateien ändern und `npm run generate` aufrufen

# Vergleichen

Man kann die generierten XML-Dateien im [generated](generated)-Ordner mit den Orginal-Dateien im [original](original)-Ordner (Diese Dateien wurden automatisch durchformatiert aber sonst nicht verändert.) schön vergleichen, indem man die jeweiligen Dateien im VS Code-Explorer mit `Ctrl+Click` selektiert und im Kontextmenü `Compare Selected` aufruft.

Bei `Elevate` gibt es nur wenige Unterschiede (Copy/Paste-Fehler in `stylemenu` und Unterschiede in der Groß-/Kleinschreibung bei den Dateinamen der Wegmarkierungssymbole). Bei `Elements` fehlen noch viele `zoom-min`-Abfragen in der Datei [part/todo.xml](part/todo.xml).

Wenn man das `oam-elevate` Repository mit Git ge-clone-t hat, sieht man im Source-Control View in der Sidebar (View -> SCM) nach dem Ändern einer Quell-Datei und dem Aufruf von `npm run generate` schön die Veränderungen in den generierten Dateien.
