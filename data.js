// @ts-check
import { readFileSync } from "fs";
import deepmerge from "deepmerge";

/**
 * Returns the content of the root XML element in the specified file.
 * The file must include an XML declaration and a root element with non-empty content.
 *
 * @param {string} filename - filename containing XML text
 * @returns {string} - the content of the root XML element
 */
export function includeRootContent(filename) {
	const template = readFileSync(filename, "utf8");
	const start = template.indexOf(">", template.indexOf(">") + 1) + 1;
	const end = template.lastIndexOf("</");
	const content = template.substring(start, end);
	return content;
}

/**
 * Returns a new object with the merged enumerable properties of both x and y.
 * If the same key is present for both x and y, the value from y will appear in the result.
 *
 * @param {object} x - first object
 * @param {object} y - second object, eventually overriding properties of the first one
 * @returns {object} - a new merged object with the merged enumerable properties of both x and y
 */
export function merge(x, y) {
	return deepmerge(x, y, {
		arrayMerge: (_target, source, _options) => source,
	});
}
