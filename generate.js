// @ts-check
import tal from "template-tal";
import format from "xml-formatter";
import { readFileSync, writeFileSync } from "fs";
import { exit } from "process";

/**
 * @param {string} templateFilename
 * @param {string} dataFilename
 * @param {string} generatedFilename
 * @returns {Promise<void>}
 */
async function generate(templateFilename, dataFilename, generatedFilename) {
	const template = readFileSync(templateFilename, "utf8");
	const { default: data } = await import("./" + dataFilename);
	const processed = await tal.process(template, data);
	const formatted = format(processed, {
		indentation: "\t",
		lineSeparator: "\n",
		whiteSpaceAtEndOfSelfclosingTag: true,
	});
	const replaced =
		formatted.replace(
			"http://mapsforge.org/renderTheme renderTheme.xsd http://xml.zope.org/namespaces/tal tal.xsd",
			"http://mapsforge.org/renderTheme https://raw.githubusercontent.com/mapsforge/mapsforge/master/resources/renderTheme.xsd",
		) + "\n";
	writeFileSync(generatedFilename, replaced, "utf8");
}

/**
 * Shows an error message and terminates the programm
 * @param {string} error
 * @returns {never}
 */
function showErrorAndExit(error) {
	console.error(error);
	console.error(
		"\nExample:\n  node generate.js ElevateTemplate.xml ElevateData.js generated/Elevate.xml\n",
	);
	exit(1);
}

const { argv } = process;
const [, , templateFilename, dataFilename, generatedFilename] = argv;
if (templateFilename === undefined)
	showErrorAndExit("No filename was given for the template XML file.");
if (dataFilename === undefined) showErrorAndExit("No filename was given for the data JS file.");
if (generatedFilename === undefined)
	showErrorAndExit("No filename was given for the generated XML file.");

generate(templateFilename, dataFilename, generatedFilename);
