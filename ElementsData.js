// @ts-check
import { merge } from "./data.js";
import elevate from "./ElevateData.js";

export default merge(elevate, {
	reduce: undefined,
	id_prefix: "elmt-",
});
